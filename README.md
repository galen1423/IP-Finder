## IP Finder

### What is IP Finder Gnome Extension?

IP Finder displays information about your public IP address (hostname, country, AS block etc) as well as show a map & flag image of the IP's geolocation this extension is also useful for informational purposes to monitor VPN geolocation and public network IP addresses.

-----



### License & Terms ![](https://gitlab.com/LinxGem33/IP-Finder/blob/master/screens/Copyleft-16.png)

IP Finder is available under the terms of the GPL-3.0 license See [`COPYING`](https://gitlab.com/LinxGem33/IP-Finder/blob/master/COPYING) for details.

----- 

### Contributions & Suggestions

Any suggestions for features and contributions to the continuing code development can be made on IP Finder's GitLab page via the issue tracker or code contributions via a pull request.
